---
title: "About"
date: 2020-06-06T19:01:01-07:00
draft: true
---

## What is everydev?

everydev was inspired by the notion that every person who wants to become a software developer, should be able to do so.

Not only should they have a path towards becoming one, they should be nurtured throughout their career by a healthy ecosystem of companies and tecnical leaders.

## Who is behind everydev?

Etienne de Bruin founded everydev as a limited liability corporation in 2017.  He is a long time technical leader and is also the founder of [7CTOs](https://7ctos.com).