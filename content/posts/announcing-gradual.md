+++ 
draft = true
date = 2020-06-06T19:16:12-07:00
title = "Announcing Gradual App"
description = "The easiest way to give micro donations"
slug = "" 
tags = ["Mobile Apps"]
categories = ["Projects"]
externalLink = ""
series = []
+++

# A super easy way to donate

We built Gradual with the idea that people want to give to their favorite causes but because life gets in the way, aren't able to write the big checks at the end of each month.

_What if you're able to tap a button at any time of day and deposit $1 to your favorite cause?_

# How it works

Download the Gradual app from the Apple App Store, or the Google Play Store.  After a quick registration of email and password, you're able to choose from a list of approved organizations to give to.

Simply select your chosen organization, and whenever you feel like it, tap the button to donate _one dollar_ towards them.  As soon as you hit the total of _ten dollars_, your credit card will be charged and the amount will be assigned to that organization.

# Fun ways to give

The best way to give is to do it at multiple times during the day.  In addition to tapping the button when you think about your organization, you can also try these ideas:

1. After your workout
2. After providing a service like a therapy session or a doctor's consultation
3. After your visit to the grocery store

Before you know it you'll have made a difference and much like a piggy bank it all adds up.

Soon we'll be in the app stores.
